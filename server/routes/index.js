var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');
var async = require('async');
var mail = require('../mailsender');
var Users = require('../dbuser');
var mongoose = require('mongoose');
mongoose.connect('mongodb://127.0.0.1:27017/usermails');

router.post('/sendlist', function(req, res){ //Sending message to cliens mail
	mail.send(req.body.mails)
	var maillist = req.body.mails;
	
	maillist.forEach(function(datalist){
		Users.findOne({mail: datalist},function(err,u) {
		    if(!u){    
		      	var user = new Users({
			      mail : datalist
		    	})
		    	user.save();
		    	console.log('Save')
	    	}
	  	})
	})
    
})


router.get('/getuserlist', function(req, res){
	var dbaselist = ['<tr><th>#</th><th>Client</th><th>read</th><th>click</th></tr>']; // empty array , for sending html page 
	Users.find({}, function(err, subjects) {
		if(err) console.log(err);
		if(!subjects) console.log('Empty')
		if(subjects){
			subjects.map(function(listuser){
				if(listuser.mail){
					dbaselist.push( '<tr><td>-</td><td>' + listuser.mail + '</td><td id=read>' + listuser.opensum + '</td><td id=click>' + listuser.clicksum + '</td></tr>');	
				}	
			})
		}
	    res.send(dbaselist);  	
	});
})




router.get('/getimage', function(req, res){ // Opened message
	var email = req.query.mail;
	Users.findOne({mail: email},function(err,u) {
	    if(u){
	      	console.log('Opened: ', u.mail)
	      	u.opensum=1;
	      	u.save();
	    }else{
	    	console.log('Opened')
	    }
	})
	res.send('ok')

	/*
	fs.readFile(path.join(__dirname, '../logo.png'), function read(err, data) {
	    if (err) { res.send('noimg') }
	    else {
	      var mail = req.body.mail;
	      console.log(mail,'<<<<<<<==============')
	          res.writeHead('200', {'Content-Type': 'image/png'});
	          res.end(data,'binary');
	        }
	})	*/
})

router.get('/redir', function(req, res){ // Clicked to link
	var email = req.query.mail;
	Users.findOne({mail: email},function(err,u) {
	    if(u){
	      	console.log('Clicked: ', u.mail)
	      	u.clicksum=1;
	      	u.save();
	    }else{
    		console.log('Clicked') //target mail
	    }
	});
	res.writeHead(301,
		{Location: 'http://onelife.systems?time='+new Date().getTime()}
	);
	res.end();
})
module.exports = router
